import { Icon, Button, Layout } from 'antd';
import PageHead from '../components/header';
import Foot from '../components/footer';
import SideBar from '../components/sider';
import { useState } from 'react';
const { Content } = Layout;

const Calculator = () => {
    const [history, setHistory] = useState(null);
    const [number, setNumber] = useState(null);
    const [result, setResult] = useState(null);
    const [arrHistory, setArrHistory] = useState([]);
    const [native, setNative] = useState(false);

    const arrBtn = ['CE', 'C', `◄`, '÷', '7', '8', '9', 'x', '4', '5', '6', '-', '1', '2', '3', '+', '±', '0', '.', '='];
    const compBtn = arrBtn.map((btn, index) => {
        return (
            <div key={index} style={{ width: '25%', display: 'inline-block' }}>
                <Button onClick={() => getValueNumber(btn)} style={{ width: '100%', height: '40px'}}>{btn}</Button>
            </div>
        )
    });

    const checkCharacter = (char) => {
        switch (char) {
            case 'x':
                setNumber(number.concat('*'));
                break;
            case '÷':
                setNumber(number.concat('/'));
                break;
            case 'CE':
                setNumber('');
                setResult(null);
                setNative(false);
                break;
            case 'C':
                setNumber('');
                setResult(null);
                setNative(false);
                break;
            case '◄':
                if (number === null || number === '') {
                    return;
                } else {
                    setNumber(number.substring(0, number.length - 1));
                }
                break;
            case '±':
                if(native === false && number !== '='){
                    setNumber('-'.concat(number));
                    setNative(!native);
                    console.log('cat chuoi');
                }else 
                    setNumber(number.substring(number.length, 1));
                    setNative(!native);
                    console.log('native');
                
            default:
                break;
        }
    }
    const operators = (numb) => {

        //  clear all
        if (result !== null) {
            setNumber('');
            setResult(null);
        }
        // check character
        else if ((numb === '±' || numb === '÷' || numb === 'x' || numb === 'CE' ||
            numb === 'C' || numb === '◄') && numb !== '=') {
            checkCharacter(numb);
        }
        else if (number === null && numb !== '=') {
            setNumber(numb);
        } else if (number !== null && number !== '=') {
            setNumber(number.concat(numb))
        }
        if (numb === '=') {
            setNumber(number);
            setNative(false);
            let historys = [...arrHistory];
            setResult(() => eval(number));
            let history = {
                expre: number,
                result: eval(number)
            }
            historys.push(history)
            setArrHistory(() => historys);
        }
    }

    const getValueNumber = (numb) => {
        operators(numb);
    }
    const showHistory = () => {
        setHistory(!history);
    }
    const clearHistory = () => {
        setArrHistory([]);
    }

    return (
        <div>
            <Layout>
                <PageHead />
                <Layout>
                    <SideBar />
                    <Content style={{ margin: '24px 16px 0' }}>
                        <div style={{ padding: 24, background: '#fff', minHeight: 500 }}>
                            <div className="counter">
                                <div className="clicker">
                                    <Icon type="calculator" style={{ fontSize: '36px', color: 'white', marginLeft: '20%' }} />
                                    <b>React Calculator</b>
                                </div>
                                <div className="calculator">
                                    <div className="express">
                                        <div className="operation">
                                            <input type="text" className="operator" defaultValue={number} />
                                        </div>
                                        <div className="result">
                                            <span>
                                                {result}
                                            </span>
                                        </div>

                                        <div className="history">
                                            <Button onClick={() => showHistory()} style={{ width: 346 }} ><span className="textHistory">&#8634;</span></Button>
                                        </div>
                                    </div>

                                    {
                                        !history
                                            ?
                                            <div className="compoentBtn">
                                                {compBtn}
                                            </div>
                                            :
                                            <div className="resultOutput">
                                                {
                                                    arrHistory.map((item, index) => (
                                                        <div key={index}>
                                                            <span style={{fontSize:18}}>{item.expre} = {item.result}</span>
                                                        </div>
                                                    ))
                                                }
                                                <div className="btnDelete">
                                                    <Button className="iconDelete" onClick={() => clearHistory()}><Icon type="delete" /></Button>
                                                </div>
                                            </div>

                                    }

                                </div>
                            </div>
                            <style jsx>
                                {`

                .counter {
                    left:25%;
                    position: relative;
                    width: 350px;
                    height: 400px;
                    border: 2px solid black
                 }
                 .clicker {
                    width:100%;
                    height: 40px;
                    background-color: black
                }
                .clicker {
                    color: #e8e8e8
                }
                .clicker b {
                    float: right;
                    margin: 5px 40px 5px 5px;
                    font-size: 24px;
                }
                .operation {
                    width: 100%;
                    float: left;
                }
                .operator{
                    font-size: 18px;
                    width: 100%;
                    height: 25px;
                    border:none;
                }
                .result {
                    margin-top: 20px;
                    float: left;
                }
                .express {
                    width: 100%;
                    height: 120px;
                    position:relative;
                }
                .history {
                    position: absolute;
                    width: 100%;
                    bottom: 0;
                }
                .btnHistory {
                    width: 100%;
                    height: 30px;
                }
                .compoentBtn {
                    width: 100%;
                }
                .textHistory {
                    font-size: 24px;
                }
                .result span {
                    font-size: 24px;
                }
                .resultOutput {
                    position: relative;
                    float:left;
                    width: 100%;
                    height: 150px;
                    overflow: auto
                }
                .btnDelete {
                    position: absolute;
                    top: 0%;
                    right:0%;
                }
                `}
                            </style>
                        </div>
                    </Content>
                </Layout>
                <Foot />
            </Layout>
        </div>
    )
}
export default Calculator;