import React, { useState, useEffect } from 'react';

import MyLayout from '../components/mylayout';
const Index = () => {
    const [count, setCount] = useState(0);

    useEffect(() => {
        document.writeln = `${count}`;
        console.log(count);
    })

    return (
        <div>
            <MyLayout>
            </MyLayout>

        </div>
    )
}

export default Index;