import { Layout } from 'antd';
import Head from '../components/header';
import Foot from '../components/footer';
import SideBar from '../components/sider';
const {Content} = Layout;
const MyLayout = () => (
    <div>
        <Layout>
            <Head />
            <Layout>
            <SideBar />
            <Content style={{ margin: '24px 16px 0' }}>
                <div style={{padding: 24, background: '#fff', minHeight: 500}}>
                    
                </div>
            </Content>
            </Layout>
            <Foot />
        </Layout>
    </div>
)
export default MyLayout;