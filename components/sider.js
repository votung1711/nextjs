import Link from 'next/link';
import { Layout, Menu, Breadcrumb, Icon } from 'antd';

const { SubMenu } = Menu;
const {  Content, Sider } = Layout;

const SideBar = () => (
            <Sider
                breakpoint="lg"
                collapsedWidth="0"
                onBreakpoint={broken => {
                    console.log(broken);
                }}
                onCollapse={(collapsed, type) => {
                    console.log(collapsed, type);
                }}
            >
                <div className="logo" />
                <Menu theme="dark" mode="inline">
                    <Menu.Item key="1">
                    <Icon type="clock-circle" />
                        <Link href="/timer">
                            <a >Timer</a>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="2">
                    <Icon type="select" />
                        <Link href="/clicker">
                            <a  >Clicker</a>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="3">
                    <Icon type="history" />
                        <Link href="/timer2">
                            <a  >Timer 2</a>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="4">
                    <Icon type="calculator" />
                        <Link href="/calculator">
                            <a >Calculator</a>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="5">
                    <Icon type="clock-circle" />
                        <Link href="/clock">
                        <a  >Clock</a>
                    </Link>
                    </Menu.Item>
                </Menu>
            </Sider>      
)
export default SideBar;