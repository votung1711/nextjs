import { Layout } from 'antd';
const { Footer } = Layout;
const Foot = () => (
    <Layout>
        <Footer style={{ textAlign: 'center' }}>Ant Design ©2019 Created by TungVT6</Footer>
    </Layout>
)
export default Foot;