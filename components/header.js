import { Layout } from 'antd';
import Head from 'next/head'
const { Header } = Layout;

const title = {
    fontSize: 40,
    color: '#fff',
    marginLeft: '45%',
}
const PageHead = () => (
    <Layout>
        <Head>
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <meta charSet="utf-8" />
        </Head>
        <Header style={{ background: 'rgb(0, 21, 41);', padding: 0 }} >
            <span style={title}>
                NextJS
            </span>
        </Header>
    </Layout>

)
export default PageHead;